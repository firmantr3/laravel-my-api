<?php 

namespace App\Traits\Service;

trait HasOptions {

    /** @var array */
    protected $options;

    /**
     * Get merged default & custom options
     *
     * @param array $options
     * @return array
     */
    public function initializeOptions($options, $defaults) {
        return array_merge($defaults, $options);
    }

    public function getOption($index, $default = null) {
        return isset($this->options[$index]) ? $this->options[index] : $default;
    }

    public function setOption($index, $value) {
        $this->options[$index] = $value;

        return $this;
    }
    
}

<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use App\QytaTrans\Services\SmsService;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendSms implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $message;

    protected $phone;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($message, $phone)
    {
        $this->message = $message;
        $this->phone = $phone;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(SmsService $sms)
    {
        $result = $sms->text($this->message)->phone($this->phone)->send();
    }
}

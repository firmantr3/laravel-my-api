<?php

namespace App\Http\Controllers;

use App\User;
use App\Services\Paginate;
use App\Filters\UsersFilter;
use Illuminate\Http\Request;
use App\Transformers\UsersTransformer;

class UsersController extends ApiController
{

    public function __construct(UsersTransformer $transformer) {
        $this->transformer = $transformer;
    }

    public function index(UsersFilter $filter) {
        $data = new Paginate(User::filter($filter));

        return $this->respondWithPagination($data);
    }
}

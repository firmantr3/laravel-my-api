<?php 

namespace App\Traits\Model;

trait DefaultConnection {

    public function getConnectionName() {
        return config('database.default');
    }
    
}

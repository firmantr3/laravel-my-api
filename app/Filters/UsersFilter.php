<?php 

namespace App\Filters;

use App\Abstracts\Filter;
use Illuminate\Support\Facades\DB;

class UsersFilter extends Filter {
    
    public function registerDate($date) {
        return $this->builder->where(DB::raw('date(created_at)'), $date);
    }

}

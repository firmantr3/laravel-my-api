<?php 

namespace App\QytaTrans\Traits\UserVerification;

use App\Jobs\SendSms;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\User;
use Illuminate\Auth\Events\Registered;

trait UserVerification {
    /** @var string */
    protected $userVerifiedAttribute = 'verified';

    /** @var string */
    protected $userVerificationCodeAttribute = 'verification_code';

    /** @var string */
    protected $verificationCode;

    /**
     * Check user verification
     *
     * @param Request $request
     * @param User $user
     * @return true|false
     */
    public function checkVerification(Request $request, User $user) {
        $verificationCode = $request->input('verification_code', null);

        if($verificationCode && $user->{$this->userVerificationCodeAttribute} == $verificationCode) {
            $this->userVerified($user);
            return true;
        }

        $this->generateVerificationCode($user);
        return false;
    }

    /**
     * Generate verification code, and update to user model
     *
     * @param User $user
     * @return void
     */
    public function generateVerificationCode(User $user) {
        if(!$user->{$this->userVerificationCodeAttribute}) {
            $this->verificationCode = rand(100000, 999999);
            $user->{$this->userVerificationCodeAttribute} = $this->verificationCode;
            $user->save();
        }
        
        SendSms::dispatch("Qyta Trans. Kode verifikasi anda: {$this->verificationCode}. Kode bersifat rahasia, jangan berikan kepada siapapun!", $user->phone);
    }

    /**
     * User is verified, update the model
     *
     * @param User $user
     * @return void
     */
    public function userVerified(User $user) {
        $user->{$this->userVerificationCodeAttribute} = null;
        
        if(!$user->{$this->userVerifiedAttribute}) {
            $user->{$this->userVerifiedAttribute} = 1;
            event(new Registered($user));
        }

        $user->save();
    }
}

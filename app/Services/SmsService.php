<?php 

namespace App\QytaTrans\Services;

use GuzzleHttp\Client;
use App\Traits\Service\HasOptions;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7;

class SmsService {
    
    use HasOptions;

    public function __construct($options = []) {
        $this->options = $this->initializeOptions($options, [
            'request_method' => 'POST',
            'payload' => [],
            'headers' => [],
        ]);
    }

    public function getApiUrl() {
        return config('sms.api_url');
    }

    public function getUser() {
        return config('sms.auth.user');
    }

    public function getPassword() {
        return config('sms.auth.password');
    }

    public function getPayload() {
        return $this->options['payload'];
    }

    public function getRequestMethod() {
        return $this->options['request_method'];
    }

    public function getHeaders() {
        return $this->options['headers'];
    }

    public function getUserKey() {
        return config('sms.key.user');
    }

    public function getPasswordKey() {
        return config('sms.key.password');
    }

    public function getMessageKey() {
        return config('sms.key.message');
    }

    public function getPhoneKey() {
        return config('sms.key.phone');
    }

    public function credentialsToPayload() {
        $this->options['payload'][$this->getUserKey()] = $this->getUser();
        $this->options['payload'][$this->getPasswordKey()] = $this->getPassword();

        return $this;
    }

    public function text($text) {
        $this->options['payload'][$this->getMessageKey()] = $text;
        return $this;
    }

    public function phone($number) {
        $this->options['payload'][$this->getPhoneKey()] = $number;
        return $this;
    }

    public function send() {
        if(!isset($this->options['payload'][$this->getMessageKey()])) {
            return null;
        }

        if(!isset($this->options['payload'][$this->getPhoneKey()])) {
            return null;
        }

        $client = new Client([
            // Base URI is used with relative requests
            'base_uri' => $this->getApiUrl(),
            // You can set any number of default request options.
            'timeout'  => 5.0,
            'headers' => $this->getHeaders(),
        ]);

        try {
            switch($this->getRequestMethod()) {
                case 'POST': 
                    $response = $client->request('POST', '', [
                        'form_params' => $this->getPayload(),
                    ]);
                break;

                case 'GET':
                    $response = $client->request('GET', '', [
                        'query' => $this->getPayload()
                    ]);
                break;

                default: 
                break;
            }
        } catch(RequestException $e) {
            return null;
        }

        if(isset($response) && $response->getStatusCode() != 200) {
            return null;
        }

        return json_decode($response->getBody()->getContents());
    }

}

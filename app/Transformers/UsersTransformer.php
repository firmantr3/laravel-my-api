<?php 

namespace App\Transformers;

use App\Abstracts\Transformer;

class UsersTransformer extends Transformer {

    protected $resourceName = 'user';

    public function transform($data) {
        return [
            'id' => $data['id'],
            'name' => $data['name'],
            'email' => $data['email'],
            'photo_url' => $data['photo_url'],
            'registered_at' => $data['created_at'],
        ];
    }
}

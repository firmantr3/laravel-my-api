<?php 

return [
    'api_url' => env('SMS_API_URL', null),
    'auth' => [
        'user' => env('SMS_AUTH_USER', null),
        'password' => env('SMS_AUTH_PASS', null),
    ],
    'key' => [
        'user' => env('SMS_USER_KEY', 'user'),
        'password' => env('SMS_PASSWORD_KEY', 'password'),
        'message' => env('SMS_MESSAGE_KEY', 'message'),
        'phone' => env('SMS_PHONE_KEY', 'phone'),
    ],
];
